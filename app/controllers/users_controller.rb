class UsersController < ApplicationController
	before_action :set_user, only: %i[ show edit update destroy ]

	def index
		@users = User.all 
	end

	def show; end

	def edit; end

	def new 
		@user = User.new
	end

	def create
		user = @user.create!(params_user)
		render json: user
	end

	def update
		@user.update!(params_user)

		render json: 'Atualizado com sucesso!'
	end

	def destroy
		@user.destroy
	end

	private 

	def set_user
		@user = User.find params[:id] 
	end

	def params_user
		params.require(:user).permit(:name, :email, :encrypted_password, :reset_password, :foto)
	end 
end