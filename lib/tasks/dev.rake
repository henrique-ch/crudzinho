namespace :dev do
    DEFAULT_PASSWORD = 123456
  
    desc "Configura o ambiente de desenvolvimento"
    task setup: :environment do
      if Rails.env.development?
        show_spinner("Apagando BD...") { %x(rails db:drop) }
        show_spinner("Criando BD...") { %x(rails db:create) }
        show_spinner("Migrando BD...") { %x(rails db:migrate) }
        show_spinner("Cadastrando alguns registros de usuarios...") { %x(rails dev:add_users) }
      else
        puts "Você não está em ambiente de desenvolvimento!"
      end
    end
  
    desc "Adiciona usuarios"
    task add_users: :environment do
        10.times do |i|
          User.create!(
            name: Faker::Internet.user_name,
            email: Faker::Internet.email,
            password: DEFAULT_PASSWORD,
            password_confirmation: DEFAULT_PASSWORD
          )
        end
    end
  
    private
  
    def show_spinner(msg_start, msg_end = "Concluído!")
      spinner = TTY::Spinner.new("[:spinner] #{msg_start}")
      spinner.auto_spin
      yield
      spinner.success("(#{msg_end})")
    end
  end 