namespace :teste do
    
  PATH_FILE = "$HOME/Downloads"
  FILE = 
  LOGAR_POSTGRES = "sudo su postgres"
  CREATE_DATABASE = "psql -U postgres -c 'create database baladapp_development' "
  DATABASE = "\c baladapp_development"
  RESTORE_DATABASE = "cd '#{PATH_FILE}' && psql -U postgres -d baladapp_development < filename.sql"

    task teste: :environment do
      if Rails.env.development?
        show_spinner("Apagando base de dados atual...") { %x(rails db:drop) }
        show_spinner("Criando nova base de dados...") { exec( "#{CREATE_DATABASE}" ) }
        show_spinner("Fazendo restore...") { exec( "#{RESTORE_DATABASE}" ) }
      else
        puts "Você não está em ambiente de desenvolvimento" 
      end
    end 

    desc "Adicionando seu usuário"
    task add_user: :environment do
          User.create!(
            name: Faker::Internet.user_name,
            email: Faker::Internet.email,
            password: DEFAULT_PASSWORD,
            password_confirmation: DEFAULT_PASSWORD
          )      
    end

    private
  
    def show_spinner(msg_start, msg_end = "Concluído!")
      spinner = TTY::Spinner.new("[:spinner] #{msg_start}")
      spinner.auto_spin
      yield
      spinner.success("(#{msg_end})")
    end
end